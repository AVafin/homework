class task_6 {
	public static void main(String args[]) {
		int n = 14;
		int current_number = 1;
		int previous_number = 1;
		int temporary;
		for(int i = 0; i < n; ++i) {
			System.out.println(current_number);
			temporary = current_number;
			current_number += previous_number; 
			previous_number = temporary;
		}
	}
}