class task_5 {
	public static void main(String args[]) {
		int factorial = 1;
		int number = 3;
		for(int i = 0; i < number; ++i) {
			factorial = factorial * (i + 1); 
		}
		System.out.println("factorial - " + factorial);
	}
}