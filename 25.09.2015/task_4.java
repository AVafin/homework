class task_2 {
	public static void main(String args[]) {
		int number = 3627;
		int count = 4; 
		String inverted = "";
		System.out.println("ordinary number - " + number);
		for(int i = 0; i < 4; ++i) {
			inverted = inverted + number % 10;
			number =  (int) Math.floor(number/10);
		}
		System.out.println("inverted number - " + inverted);
	}
}