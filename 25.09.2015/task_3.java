class task_3 {
	public static void main(String args[]) {
		int number = 36279765;
		int num_count = 8;
		int even = 0;
		int odd = 0;
		for(int i = 0; i < num_count; ++i) {
			if(number % 10 % 3 == 0 && number % 10 % 2 != 0) {
				odd = odd + 1;
				System.out.println(number + " - odd");
			} else if(number % 10 % 2 == 0) {
				even = even + 1;
				System.out.println(number + " - even");
			} else {
				odd = odd + 1;
				System.out.println(number + " - odd");
			}
			number = (int) Math.floor(number/10);
		}
		System.out.println("even numbers - " + even);
		System.out.println("odd numbers - " + odd);
	}
}