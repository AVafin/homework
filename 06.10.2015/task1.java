import java.util.*;
public class task1 {
	public static void main(String[] args) {
		int min = 1000;
		int new_min = 1000;
		int[] rand = new int [10];
		int[] increase = new int [10];
		int[] decrease = new int [10];
		int count = 0;
		Random random = new Random();
		System.out.println("TASK 1.1:");
		for(int m = 0; m < rand.length; m++) {
			increase[m] = m;
			System.out.print(increase[m] + " ");
		}
		System.out.println();
		System.out.println("TASK 1.2:");
		for(int n = rand.length - 1; n > 0; n--) {
			decrease[n] = n;
			System.out.print(decrease[n] + " ");
		}
		System.out.println();
		System.out.println("TASK 1.3:");
		for(int i = 0; i < rand.length; i++) {
			rand[i] = random.nextInt(100);
			System.out.print(rand[i] + " ");
		}
		System.out.println();
	}
}